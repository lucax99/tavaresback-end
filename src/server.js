const express = require('express');
const routes = require('./routes');
var cors = require('cors')


const app = express();

require('./database')
app.use(cors())
app.use(express.json());
app.use(routes)
app.use(cors())

app.listen(8000)